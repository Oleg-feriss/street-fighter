import { createElement } from '../helpers/domHelper';

export function createFighterPreview(fighter, position) {
  const positionClassName = position === 'right' ? 'fighter-preview___right' : 'fighter-preview___left';
  const fighterElement = createElement({
    tagName: 'div',
    className: `fighter-preview___root ${positionClassName}`,
  });
  
  if (fighter) {
    const fighterImage = createFighterImage(fighter);
    const fighterInfo = createFighterInfo(fighter);
    fighterElement.appendChild(fighterImage);
    fighterElement.appendChild(fighterInfo);
  };
  
  return fighterElement;
}

export function createFighterImage(fighter) {
  const { source, name } = fighter;
  const attributes = { 
    src: source, 
    title: name,
    alt: name 
  };
  const imgElement = createElement({
    tagName: 'img',
    className: 'fighter-preview___img',
    attributes,
  });

  return imgElement;
}

function createFighterInfo(fighter) {
  const { name, health, attack, defense } = fighter;
  const element = document.createElement('p');
   
  const text = document.createTextNode(`Name: ${name}; Health: ${health}; Attack: ${attack}; Defence: ${defense}`);
  element.appendChild(text);
  
  return element;
}