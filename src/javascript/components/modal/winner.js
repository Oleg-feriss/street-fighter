import { createFighterImage } from '../fighterPreview';
import { showModal } from './modal';

export function showWinnerModal(fighter) {
  const winnerImage = createFighterImage(fighter);
  const winnerElement = {
    title: `${fighter.name.toUpperCase()} WINS!`,
    bodyElement: winnerImage,
  };
  
  showModal(winnerElement);

  reloadInterval(1500);
}

function reloadInterval(time){
	setTimeout(function(){
		location.reload();
	}, time);
}